package com.example.mposplugin;

import java.util.ArrayList;
import java.util.List;

 

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
 
import android.widget.TextView;

public class DeviceAdapter extends BaseAdapter {
	private List<BluetoothDevice> mNodeList = new ArrayList<BluetoothDevice>();
	Context c;
	
	public DeviceAdapter(Context c,ArrayList<BluetoothDevice> data){
		this.c=c;
		this.mNodeList=data;
	}

    
    @Override
    public int getCount() {            
        return mNodeList.size();
    }

    @Override
    public Object getItem(int location) {
        return mNodeList.get(location);
    }

    @Override
    public long getItemId(int location) {
        return location;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get layout to populate
        View v = convertView;
        TextView tvDeviceName = null;
        tvDeviceName=new TextView(c);
        if (v == null) {
            LayoutInflater vi = LayoutInflater.from(c);
            tvDeviceName=new TextView(c);
            v = tvDeviceName;
        }
        
        // Populate the layout with new data       
       
       tvDeviceName.setText(mNodeList.get(position).getName());
        
        return v;        
    }

 
    
    public void clear() {
        mNodeList.clear();
    }
    
    public BluetoothDevice find(String address) {
        for (BluetoothDevice d : mNodeList) {
            if (address.equals(d.getAddress())) return d;
        }
        
        return null;
    }
}
