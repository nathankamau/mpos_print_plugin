package com.example.mposplugin;

public class DeviceNode {
	  private String mName;
      private String mAddress;
      private int mIconResId;
      public DeviceNode(){
    	  
      }
      public DeviceNode(String name, String address ) {       
          mName = name;
          mAddress = address;            
      }
      
      public String getName() {
          return mName;
      }
      
      public void setName(String name) {
          mName = name;           
      }
      
      public String getAddress() {
          return mAddress;
      } 
             
      public int getIcon() {
          return mIconResId;
      }
      
      public void setIcon(int resId) {
          mIconResId = resId;
      }        
}
