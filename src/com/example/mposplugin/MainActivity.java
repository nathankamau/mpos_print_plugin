package com.example.mposplugin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import com.datecs.api.printer.Printer;
import com.datecs.api.printer.PrinterInformation; 
import com.datecs.api.printer.ProtocolAdapter; 
 
 
 
 

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
BluetoothAdapter bluetoothAdapter;
final int REQUEST_ENABLE_BT=10;
ListView lstBTDevices;
Button btnGetDevices,btnStartDiscovery;
DeviceAdapter devicesAdapter;
ArrayList<DeviceNode> devices;
DeviceNode bluetoothElement;
Set<BluetoothDevice>pairedDevices;
private static final int REQUEST_GET_DEVICE = 0; 
private PrinterInformation mPrinterInfo;
private static final String LOG_TAG = "PrinterSample"; 
private Printer mPrinter;
  BluetoothSocket mBluetoothSocket;
  private PrinterServer mPrinterServer;
  private static final boolean DEBUG = true;
  private Socket mPrinterSocket;
  boolean mRestart=true;
  private ProtocolAdapter mProtocolAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		lstBTDevices=(ListView)findViewById(R.id.lstBlueToothDevices);
		btnGetDevices=(Button)findViewById(R.id.btnGetDevices);
		btnStartDiscovery=(Button)findViewById(R.id.btnStartDiscovery);
		devices=new ArrayList<DeviceNode>();
	 
		bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
		lstBTDevices.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1, int arg2,
					long arg3) {
				 DeviceNode node=(DeviceNode)adapter.getItemAtPosition(arg2);
				 Toast.makeText(getBaseContext(), node.getName(), Toast.LENGTH_LONG).show();
				createConnection(node.getAddress());
			}
		
		});
		if(bluetoothAdapter!=null){
			//check if blue tooth is enabled
			if(!bluetoothAdapter.isEnabled()){
				 Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}else{
				if(bluetoothAdapter.startDiscovery()){
					Toast.makeText(getBaseContext(), "Discovery started", Toast.LENGTH_LONG).show();
					IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
					registerReceiver(mReceiver, filter);
					Log.d("Started Discovery","Started"); 
					 
				}
			}
			// Register the BroadcastReceiver
		 
		}else{
			
		}
		 
	}
	 private synchronized void closeActiveConnection() {
	        closePrinterConnection();
	        closeBlutoothConnection();
	        closeNetworkConnection();  
	        closePrinterServer();
	    }
	 private synchronized void closePrinterConnection() {
	        if (mPrinter != null) {
	            mPrinter.release();
	        }
	        
	        if (mProtocolAdapter != null) {
	            mProtocolAdapter.release();
	        }
	    }
	 private synchronized void closePrinterServer() {
	    	closeNetworkConnection();
	    	
	        // Close network server
	        PrinterServer ps = mPrinterServer;
	        mPrinterServer = null;
	        if (ps != null) {
	            if (DEBUG) Log.d(LOG_TAG, "Close Network server");
	            try {
	                ps.close();
	            } catch (IOException e) {                
	                e.printStackTrace();
	            }            
	        }     
	    }
	    
	  private synchronized void closeNetworkConnection() {
	        // Close network connection
	        Socket s = mPrinterSocket;
	        mPrinterSocket = null;
	        if (s != null) {
	            if (DEBUG) Log.d(LOG_TAG, "Close Network socket");
	            try {
	                s.shutdownInput();
	                s.shutdownOutput();
	                s.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }            
	        }
	    }
	 private synchronized void closeBlutoothConnection() {        
	        // Close Bluetooth connection 
	        BluetoothSocket s = mBluetoothSocket;
	        mBluetoothSocket = null;
	        if (s != null) {
	            if (DEBUG) Log.d(LOG_TAG, "Close Blutooth socket");
	            try {
	                s.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }        
	    }
	    
    public synchronized void waitForConnection() {
        closeActiveConnection();
        
        // Show dialog to select a Bluetooth device. 
        startActivityForResult(new Intent(this, MainActivity.class), REQUEST_GET_DEVICE);
        
        // Start server to listen for network connection.
        try {
            mPrinterServer = new PrinterServer(new PrinterServerListener() {                
                @Override
                public void onConnect(Socket socket) {
                    if (DEBUG) Log.d(LOG_TAG, "Accept connection from " + socket.getRemoteSocketAddress().toString());
                    
                    // Close Bluetooth selection dialog
                    finishActivity(REQUEST_GET_DEVICE);                    
                    
                    mPrinterSocket = socket;
                    try {
                        InputStream in = socket.getInputStream();
                        OutputStream out = socket.getOutputStream();
                        initPrinter(in, out);
                    } catch (IOException e) {   
                    	e.printStackTrace();
                        error("Failed to init printer. " + e.getMessage(), mRestart);
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 // The listener for all printer events
 	private final ProtocolAdapter.ChannelListener mChannelListener = new ProtocolAdapter.ChannelListener() {
         @Override
         public void onReadEncryptedCard() {
             //toast(getString(R.string.msg_read_encrypted_card));
         }
         
         @Override
         public void onReadCard() {
             //readMagstripe();            
         }
         
         @Override
         public void onReadBarcode() {
             //readBarcode(0);
         }
         
         @Override
         public void onPaperReady(boolean state) {
             if (state) {
                 toast("Paper ready");
             } else {
                 toast("No paper");
             }
         }
         
         @Override
         public void onOverHeated(boolean state) {
             if (state) {
                 toast("Over Heating");
             }
         }
                
         @Override
         public void onLowBattery(boolean state) {
             if (state) {
                 toast("Battery Low");
             }
         }
     };
    protected void initPrinter(InputStream inputStream, OutputStream outputStream) throws IOException {
        mProtocolAdapter = new ProtocolAdapter(inputStream, outputStream);
       
        if (mProtocolAdapter.isProtocolEnabled()) {
            final ProtocolAdapter.Channel channel = mProtocolAdapter.getChannel(ProtocolAdapter.CHANNEL_PRINTER);
            channel.setListener(mChannelListener);
            // Create new event pulling thread
            new Thread(new Runnable() {                
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        
                        try {
                            channel.pullEvent();
                        } catch (IOException e) {
                        	e.printStackTrace();
                            error(e.getMessage(), mRestart);
                            break;
                        }
                    }
                }
            }).start();
            mPrinter = new Printer(channel.getInputStream(), channel.getOutputStream());
        } else {
            mPrinter = new Printer(mProtocolAdapter.getRawInputStream(), mProtocolAdapter.getRawOutputStream());
        }
        
        mPrinterInfo = mPrinter.getInformation();
        
        runOnUiThread(new Runnable() {          
            @Override
            public void run() {
            	toast("Setting up printer name and icon");
//                ((ImageView)findViewById(R.id.icon)).setImageResource(R.drawable.icon);
//                ((TextView)findViewById(R.id.name)).setText(mPrinterInfo.getName());
            }
        });
    }
    
    
	public void createConnection(final String address) {
		
		 doJob(new Runnable() {           
	            @Override
	            public void run() {                 
	                BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);                    
	                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	                InputStream in = null;
	                OutputStream out = null;
	            bluetoothAdapter.cancelDiscovery();
	                
	                try {
	                       Log.d("Connecting","Connect to " + device.getName());
	                    mBluetoothSocket = device.createRfcommSocketToServiceRecord(uuid);
	                    mBluetoothSocket.connect();
	                    in = mBluetoothSocket.getInputStream();
	                    out = mBluetoothSocket.getOutputStream();
	                    
	                } catch (IOException e) {    
	                	e.printStackTrace();
	                    error("Failed to connect  " +  e.getMessage(), true);
	                    return;
	                } finally{
	                	error("Connected Successfully",true);
	                }
	                try {
	                    initPrinter(in, out);
	                } catch (IOException e) {
	                	e.printStackTrace();
	                    error("Failed to initialize" +  e.getMessage(), mRestart);
	                    return;
	                }
	                
	            }
	        }, "Connecting to device"); 
		
		
		
	}
	private void printTestMessage(final String message){
		doJob(new Runnable(){

			@Override
			public void run() {
				try {
					mPrinter.reset();
					   mPrinter.setPageRegion(0, 0, 160, 320, Printer.PAGE_LEFT);            
	                    mPrinter.setPageXY(0, 4);            
	                    mPrinter.printTaggedText("{reset}{center}{b}"+ message +"{br}");
	                    mPrinter.drawPageRectangle(0, 0, 160, 32, Printer.FILL_INVERTED);            
	                    mPrinter.setPageXY(0, 34);
	                    mPrinter.printTaggedText("{reset}Text printed from left to right" +
	                            ", feed to bottom. Starting point in left top corner of the page.{br}");
	                    mPrinter.drawPageFrame(0, 0, 160, 320, Printer.FILL_BLACK, 1);
                    mPrinter.selectStandardMode();
                    mPrinter.feedPaper(30);   
                    mPrinter.flush();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}},"Test Message ");
	}
	
	private void printPage() {
	    doJob(new Runnable() {           
            @Override
            public void run() {
        	    if (mPrinterInfo == null || !mPrinterInfo.isPageModeSupported()) {
        	        dialog(R.drawable.ic_launcher, 
        	                 "Title warning", 
        	                "Unsupported page fromat");
        	    }
        	            
                try {
                   
                    mPrinter.reset();            
                    mPrinter.selectPageMode();   
                    
                    mPrinter.setPageRegion(0, 0, 160, 320, Printer.PAGE_LEFT);            
                    mPrinter.setPageXY(0, 4);            
                    mPrinter.printTaggedText("{reset}{center}{b}PARAGRAPH I{br}");
                    mPrinter.drawPageRectangle(0, 0, 160, 32, Printer.FILL_INVERTED);            
                    mPrinter.setPageXY(0, 34);
                    mPrinter.printTaggedText("{reset}Text printed from left to right" +
                            ", feed to bottom. Starting point in left top corner of the page.{br}");
                    mPrinter.drawPageFrame(0, 0, 160, 320, Printer.FILL_BLACK, 1);
                    
                    mPrinter.setPageRegion(160, 0, 160, 320, Printer.PAGE_TOP);            
                    mPrinter.setPageXY(0, 4);            
                    mPrinter.printTaggedText("{reset}{center}{b}PARAGRAPH II{br}");
                    mPrinter.drawPageRectangle(160 - 32, 0, 32, 320, Printer.FILL_INVERTED);            
                    mPrinter.setPageXY(0, 34);
                    mPrinter.printTaggedText("{reset}Text printed from top to bottom" +
                            ", feed to left. Starting point in right top corner of the page.{br}");
                    mPrinter.drawPageFrame(0, 0, 160, 320, Printer.FILL_BLACK, 1);
                    
                    mPrinter.setPageRegion(160, 320, 160, 320, Printer.PAGE_RIGHT);            
                    mPrinter.setPageXY(0, 4);            
                    mPrinter.printTaggedText("{reset}{center}{b}PARAGRAPH III{br}");
                    mPrinter.drawPageRectangle(0, 320 - 32, 160, 32, Printer.FILL_INVERTED);            
                    mPrinter.setPageXY(0, 34);
                    mPrinter.printTaggedText("{reset}Text printed from right to left" +
                            ", feed to top. Starting point in right bottom corner of the page.{br}");
                    mPrinter.drawPageFrame(0, 0, 160, 320, Printer.FILL_BLACK, 1);
                    
                    mPrinter.setPageRegion(0, 320, 160, 320, Printer.PAGE_BOTTOM);            
                    mPrinter.setPageXY(0, 4);            
                    mPrinter.printTaggedText("{reset}{center}{b}PARAGRAPH IV{br}");
                    mPrinter.drawPageRectangle(0, 0, 32, 320, Printer.FILL_INVERTED);            
                    mPrinter.setPageXY(0, 34);
                    mPrinter.printTaggedText("{reset}Text printed from bottom to top" +
                            ", feed to right. Starting point in left bottom corner of the page.{br}");
                    mPrinter.drawPageFrame(0, 0, 160, 320, Printer.FILL_BLACK, 1);
                    
                    mPrinter.printPage();
                    mPrinter.selectStandardMode();
                    mPrinter.feedPaper(110);   
                    mPrinter.flush();  
                } catch (IOException e) {
                	e.printStackTrace();
                    error("Failledddd" + e.getMessage(), true);            
                }
            }
	    },"Print Page");
    }
	 private void dialog(final int iconResId, final String title, final String msg) {
	        runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
	                builder.setIcon(iconResId);
	                builder.setTitle(title);
	                builder.setMessage(msg);
	                
	                AlertDialog dlg = builder.create();                
	                dlg.show();             
	            }           
	        });             
	    }
	
		private void printText() {
		    doJob(new Runnable() {           
	            @Override
	            public void run() {
	        		StringBuffer sb = new StringBuffer();
	        		sb.append("{reset}{center}{w}{h}RECEIPT");
	                sb.append("{br}");
	                sb.append("{br}");
	                sb.append("{reset}1. {b}First item{br}");
	                sb.append("{reset}{right}{h}$0.50 A{br}");
	                sb.append("{reset}2. {u}Second item{br}");
	                sb.append("{reset}{right}{h}$1.00 B{br}");
	                sb.append("{reset}3. {i}Third item{br}");
	                sb.append("{reset}{right}{h}$1.50 C{br}");
	                sb.append("{br}");
	                sb.append("{reset}{right}{w}{h}TOTAL: {/w}$3.00  {br}");            
	                sb.append("{br}");
	                sb.append("{reset}{center}{s}Thank You!{br}");
	                
	            	try {   
	            	    if (DEBUG) Log.d(LOG_TAG, "Print Text");
	            		mPrinter.reset();            		
	                    mPrinter.printTaggedText(sb.toString());                    
	                    mPrinter.feedPaper(110); 
	                    mPrinter.flush();                                          		
	            	} catch (IOException e) {
	            		e.printStackTrace();
	            	    error("Error Printing Receipt " + e.getMessage(), mRestart);    		
	            	}
	            }
		    }, "Print New Receipt");
		}
		
	
	
	 private void printSelfTest() {        
	        doJob(new Runnable() {           
	            @Override
	            public void run() {
	                try {           
	                      Log.d(LOG_TAG, "Print Self Test");
	                    mPrinter.printSelfTest();  
	                    mPrinter.flush();
	                } catch (IOException e) {
	                	e.printStackTrace();
	                    error("Failed to print test. " + e.getMessage(), true);
	                }
	            }
	        }, "Printing Self Test");		
		}
    private void doJob(final Runnable job, final String message) {
        // Start the job from main thread
        runOnUiThread(new Runnable() {            
            @Override
            public void run() {
                // Progress dialog available due job execution
                final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
                dialog.setTitle("Please Wait");
                dialog.setMessage(message);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                
                Thread t = new Thread(new Runnable() {            
                    @Override
                    public void run() {                
                        try {
                            job.run();
                        } finally {
                            dialog.dismiss();
                        }
                    }
                });
                t.start();   
            }
        });                     
    }
	
	
	
	  private void error(final String text, boolean resetConnection) {        
	        if (resetConnection) {
	            runOnUiThread(new Runnable() {
	                @Override
	                public void run() {        
	                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();                
	                }           
	            });
	                
	             
	        }
	    }
	
	
	// Create a BroadcastReceiver for ACTION_FOUND
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction();
	        // When discovery finds a device
	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	            // Get the BluetoothDevice object from the Intent
	            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	            // Add the name and address to an array adapter to show in a ListView
	            bluetoothElement=new DeviceNode(device.getName(), device.getAddress());
	            devices.add(bluetoothElement);
	            devicesAdapter=new DeviceAdapter(getBaseContext(), devices);
	            lstBTDevices.setAdapter(devicesAdapter);
	            Toast.makeText(getBaseContext(), "Discovered"+device.getName(), Toast.LENGTH_LONG).show();
				
	        }
	    }

		 
	};
	 // Don't forget to unregister during onDestroy
	public void onClick(View view){
		switch(view.getId()){
		case R.id.btnGetDevices:
//			pairedDevices=bluetoothAdapter.getBondedDevices();
//			// If there are paired devices
//			if (pairedDevices.size() > 0) {
//			    // Loop through paired devices
//			    for (BluetoothDevice device : pairedDevices) {
//			        // Add the name and address to an array adapter to show in a ListView
//			    	  bluetoothElement=new DeviceNode(device.getName(), device.getAddress());
//			            devices.add(bluetoothElement);
//			          
//			    }
//			}
//			 devicesAdapter=new DeviceAdapter(getBaseContext(), devices);
//			lstBTDevices.setAdapter(devicesAdapter);
//		
			bluetoothAdapter.cancelDiscovery();
			unregisterReceiver(mReceiver);
			Toast.makeText(getBaseContext(), "Start Printing", Toast.LENGTH_LONG).show();
			
			//printTestMessage("Testing print via bluetooth");
			
			printText();
			break;
		case R.id.btnStartDiscovery:
			if(bluetoothAdapter.startDiscovery()){
				Toast.makeText(getBaseContext(), "Discovery started", Toast.LENGTH_LONG).show();
				IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
				registerReceiver(mReceiver, filter);
				 
			}
//			waitForConnection();
			break;
		}
	}
	
	  private void toast(final String text) {
	        runOnUiThread(new Runnable() {            
	            @Override
	            public void run() {
	                if (!isFinishing()) {
	                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
	                }
	            }
	        });
	    }
@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);
		if(requestCode==REQUEST_ENABLE_BT){
			if(resultCode==RESULT_OK){
				if(bluetoothAdapter.startDiscovery()){
					Toast.makeText(getBaseContext(), "Discovery started", Toast.LENGTH_LONG).show();
					IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
					registerReceiver(mReceiver, filter);
					bluetoothAdapter.cancelDiscovery();
				}
				Toast.makeText(getBaseContext(), "BT enabled Successfully", Toast.LENGTH_LONG).show();
			}else{
				Toast.makeText(getBaseContext(), "BT enabling cancelled", Toast.LENGTH_LONG).show();
			}
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
