package com.example.mposplugin;

import java.net.Socket;

public interface PrinterServerListener {
    public void onConnect(Socket socket);
}
