 package com.example.mposplugin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.datecs.api.printer.Printer;
import com.datecs.api.printer.PrinterInformation;
import com.datecs.api.printer.ProtocolAdapter;
 

import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MposPrintPlugin extends CordovaPlugin {

	BluetoothAdapter bluetoothAdapter;
	private Socket mPrinterSocket;
	private PrinterServer mPrinterServer;
	DeviceNode bluetoothElement;

	BluetoothDevice pairingDevice;
	

	ArrayList<BluetoothDevice> devices;
	ListView lst_printers;
	DeviceAdapter devicesAdapter;
	BluetoothSocket mBluetoothSocket;
	final int REQUEST_ENABLE_BT = 10, REQUEST_ENABLE_BT_DISCOVER = 11;
	JSONObject bill_item;
	boolean mRestart = true;
	private ProtocolAdapter mProtocolAdapter;
	private Printer mPrinter;
	private PrinterInformation mPrinterInfo;
	String print_message;
	private static final String LOG_TAG = "Mpos Printer ";
	boolean DEBUG = true;
	JSONArray print_items;
	
	Dialog printer_dialog;
	public BroadcastReceiver mReceiver;

	Set<BluetoothDevice> pairedDevices;

	String printer_mac_address;
	CallbackContext mycallback;
	JSONObject receipt_details=new JSONObject();
	String company_name="",address="",pin="",vat_no="",telephone="",email="",date="",time="",cashier_name="",transaction="",register="",tax_amount="",tax_percentage="",cash_rendered="",change="",total="";
	String sales_type="",customer_names="";
	@Override
	public boolean execute(String action, JSONArray args,
			final CallbackContext callbackContext) throws JSONException {
		print_items = new JSONArray();
		mycallback = callbackContext;
	
		if (action.equals("print_mpos_receipt")) {
			print_items = args.getJSONArray(0);
			Log.e("Array size", "" + print_items.length());
			printer_mac_address = args.getString(1);
			receipt_details=args.getJSONObject(2);
			Log.e("Console :: receipt details", receipt_details.toString());
			print_message = args.getString(0);
			try{
				sales_type=receipt_details.getString("sales_type");
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("Error getting sales type .. Console ::>",""+e.getMessage());
			} 
			//get values from object

	try {	
				
				company_name=receipt_details.getString("company_name");
				address=receipt_details.getString("address");
				pin=receipt_details.getString("pin");
				vat_no=receipt_details.getString("vat_no");
				telephone=receipt_details.getString("telephone");
				email=receipt_details.getString("email");
				date=receipt_details.getString("date");
				time=receipt_details.getString("time");
				cashier_name=receipt_details.getString("cashier_name");
				transaction=receipt_details.getString("transaction");
				register=receipt_details.getString("register");
				tax_amount=receipt_details.getString("tax_amount");
				tax_percentage=receipt_details.getString("tax_per");
				total=receipt_details.getString("total");
				Log.e("Console :: tax %", tax_percentage);
				if(sales_type.equals("1")){
					cash_rendered=receipt_details.getString("cash_rendered");				
					change=receipt_details.getString("change");
					
					
					Log.e("Console total::>",""+total);
				}else{
					customer_names=receipt_details.getString("customer_names");
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("Error Converitng ::>",""+e.getMessage());
			}finally{
				Log.e("Console :: Total===>", tax_percentage);
			}
		 
		 
			
			// or Context context=cordova.getActivity().getApplicationContext();

			// call printer method
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					// Intent i=new Intent(this));
					bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
					if (!bluetoothAdapter.isEnabled()) {
						Intent enableBtIntent = new Intent(
								BluetoothAdapter.ACTION_REQUEST_ENABLE);
						cordova.setActivityResultCallback(MposPrintPlugin.this);
						cordova.getActivity().startActivityForResult(
								enableBtIntent, REQUEST_ENABLE_BT);
					} else {
						createConnection(printer_mac_address);
					}
					// print_mpos_receipt(print_message,callbackContext);

				}
			});
			return true;
		} else if (action.equals("set_up_printer")) {
			devices = new ArrayList<BluetoothDevice>();

			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					// callbackContext.error("this is me trying the syscronos by p[assin an error");
					//
					try {
						// mycallback.success("Success form plugins");
						setUpPrinter();
					} catch (Error e) {
						Log.e("Console ::Exception error", e.toString());
					}

				}
			});
			return true;
		}else if(action.equals("test")){
			
		}
		return false;
	}

	@Override
	public void onPause(boolean multitasking) {
		if (mReceiver != null) {
			Log.e("Console ::op Pause",
					"Pausing Applcication  and closing broadcast");
			cordova.getActivity().unregisterReceiver(mReceiver);
		}

		if (mPairReceiver != null) {
			// cordova.getActivity().unregisterReceiver(mPairReceiver);
		}
	}

	@Override
	public void onResume(boolean multitasking) {
		Log.e("Console  :: OnResune",
				"Resuming Applcication  and closing broadcast");
		IntentFilter intent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		cordova.getActivity().registerReceiver(mReceiver, intent);

	}

	/**
	 * check if device is paired
	 * 
	 * @param d
	 *            --device
	 * @return --true if paired else not paired
	 */
	public boolean checkIfPaired(BluetoothDevice d) {

		for (BluetoothDevice bt : pairedDevices) {

			if (bt.getAddress().equals(d.getAddress())) {

				Toast.makeText(cordova.getActivity(),
						"Devices Paired" + bt.getName(), Toast.LENGTH_LONG)
						.show();
				return true;
			}
		}
		return false;
	}

	/**
	 * broadcast for pairing devices...
	 */
	private final BroadcastReceiver mPairReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			JSONObject responseObject = new JSONObject();
			final JSONArray responseArray = new JSONArray();
			if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
				final int state = intent
						.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE,
								BluetoothDevice.ERROR);
				final int prevState = intent.getIntExtra(
						BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE,
						BluetoothDevice.ERROR);

				if (state == BluetoothDevice.BOND_BONDED
						&& prevState == BluetoothDevice.BOND_BONDING) {
					Toast.makeText(cordova.getActivity(),
							"Paired Successfully", Toast.LENGTH_LONG).show();

					responseArray.put(true);
					responseArray.put(pairingDevice.getAddress());
					responseArray.put(pairingDevice.getName());
					// close dialog
					printer_dialog.dismiss();
					Log.e("Console",
							"Successfully returnded " + pairingDevice.getName());

					mycallback.sendPluginResult(new PluginResult(Status.OK,
							responseArray));
					// mycallback.success(responseArray);

				} else if (state == BluetoothDevice.BOND_NONE
						&& prevState == BluetoothDevice.BOND_BONDED) {
					Toast.makeText(cordova.getActivity(), " unpairing",
							Toast.LENGTH_LONG).show();

				}

			}
		}
	};// end of pair receiver

	/**
	 * Set up printers
	 */
	private void setUpPrinter() {

		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		// get paired Devices
		pairedDevices = bluetoothAdapter.getBondedDevices();

		lst_printers = new ListView(cordova.getActivity());
		if (!bluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			cordova.setActivityResultCallback(MposPrintPlugin.this);
			cordova.getActivity().startActivityForResult(enableBtIntent,
					REQUEST_ENABLE_BT_DISCOVER);
		} else {
			if (bluetoothAdapter.isDiscovering()) {
				bluetoothAdapter.cancelDiscovery();
			}
			if (bluetoothAdapter.startDiscovery()) {
				// Create a BroadcastReceiver for ACTION_FOUND
				mReceiver = new BroadcastReceiver() {

					@Override
					public void onReceive(Context context, Intent intent) {

						Toast.makeText(cordova.getActivity(),
								"Broadcase Receiver", Toast.LENGTH_LONG).show();

						String action = intent.getAction();
						// When discovery finds a device
						if (BluetoothDevice.ACTION_FOUND.equals(action)) {
							// Get the BluetoothDevice object from the Intent
							BluetoothDevice device = intent
									.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
							if (!checkIfPaired(device)) {
								devices.add(device);
							}
							Log.e("Device found", device.getName());
							devicesAdapter = new DeviceAdapter(
									cordova.getActivity(), devices);
							lst_printers.setAdapter(devicesAdapter);

						}
					}
				};
				// Toast.makeText(cordova.getActivity(), "Started Discovery",
				// Toast.LENGTH_LONG).show();

				IntentFilter filter = new IntentFilter(
						BluetoothDevice.ACTION_FOUND);
				cordova.getActivity().registerReceiver(mReceiver, filter);
				Log.d("Started Discovery", "Started");

			} else {
				Toast.makeText(cordova.getActivity(),
						"Couldnot Start Discorable", Toast.LENGTH_LONG).show();
			}
		}
		printer_dialog = new Dialog(cordova.getActivity());

		// lst_printers.setAdapter(adapter);
		printer_dialog.setTitle("Printers Available");
		// printer_dialog.setCancelable(false);

		printer_dialog.setContentView(lst_printers);

		lst_printers.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1,
					int position, long arg3) {
				Toast.makeText(cordova.getActivity(), "Clicked",
						Toast.LENGTH_LONG).show();
				bluetoothAdapter.cancelDiscovery();

				IntentFilter intent = new IntentFilter(
						BluetoothDevice.ACTION_BOND_STATE_CHANGED);
				cordova.getActivity().registerReceiver(mPairReceiver, intent);

				BluetoothDevice device = (BluetoothDevice) adapter
						.getItemAtPosition(position);
				pairingDevice = device;
				pairDevice(device);

			}
		});
		printer_dialog.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				if (bluetoothAdapter.isDiscovering()) {
					bluetoothAdapter.cancelDiscovery();
				}
				JSONArray responseArray=new JSONArray();
				responseArray.put(false);
				mycallback.sendPluginResult(new PluginResult(Status.ERROR,responseArray));

				Toast.makeText(cordova.getActivity(), "Dismis Dialog",
						Toast.LENGTH_LONG).show();

			}
		});
		printer_dialog.show();

	}

	/**
	 * unpair devices
	 * 
	 * @param device
	 */
	private void unpairDevice(BluetoothDevice device) {
		try {
			Method method = device.getClass().getMethod("removeBond",
					(Class[]) null);
			method.invoke(device, (Object[]) null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * pair devices
	 * 
	 * @param device
	 */
	private void pairDevice(BluetoothDevice device) {
		try {
			Method method = device.getClass().getMethod("createBond",
					(Class[]) null);
			method.invoke(device, (Object[]) null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == cordova.getActivity().RESULT_OK) {
				createConnection(printer_mac_address);
			}
		} else if (requestCode == REQUEST_ENABLE_BT_DISCOVER) {
			if (resultCode == cordova.getActivity().RESULT_OK) {
				if (bluetoothAdapter.startDiscovery()) {
					IntentFilter filter = new IntentFilter(
							BluetoothDevice.ACTION_FOUND);
					cordova.getActivity().registerReceiver(mReceiver, filter);
					Log.d("Started Discovery", "Started");

				}
			}
		}
		Log.e("Cordova Results", "On activity results==" + resultCode);
		Toast.makeText(cordova.getActivity(),
				"On activity Result" + resultCode, Toast.LENGTH_LONG).show();
	}

	public void print_mpos_receipt(String message, CallbackContext callback) {
		if (message.length() > 0) {
			Toast.makeText(cordova.getActivity(), "Test Toast",
					Toast.LENGTH_LONG).show();
			callback.success("Printing you message from android ");
		} else {
			callback.error("Error printing your message");
		}
	}

	public void createConnection(final String address) {
		closeActiveConnection();
		doJob(new Runnable() {
			@Override
			public void run() {
				bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
				BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
				UUID uuid = UUID
						.fromString("00001101-0000-1000-8000-00805F9B34FB");
				InputStream in = null;
				OutputStream out = null;
				bluetoothAdapter.cancelDiscovery();

				try {
					Log.d("Connecting", "Connect to " + device.getName());
					mBluetoothSocket = device
							.createRfcommSocketToServiceRecord(uuid);
					mBluetoothSocket.connect();
					in = mBluetoothSocket.getInputStream();
					out = mBluetoothSocket.getOutputStream();

				} catch (IOException e) {
					e.printStackTrace();
					error("Failed to connect  " + e.getMessage(), true);
					return;
				} finally {
					error("Connected Successfully", true);
				}
				try {
					initPrinter(in, out);
				} catch (IOException e) {
					e.printStackTrace();
					error("Failed to initialize" + e.getMessage(), mRestart);
					return;
				}

			}
		}, "Connecting to device");

	}// end of create connection

	protected void initPrinter(InputStream inputStream,
			OutputStream outputStream) throws IOException {
		mProtocolAdapter = new ProtocolAdapter(inputStream, outputStream);

		if (mProtocolAdapter.isProtocolEnabled()) {
			final ProtocolAdapter.Channel channel = mProtocolAdapter
					.getChannel(ProtocolAdapter.CHANNEL_PRINTER);
			// channel.setListener(mChannelListener);
			// Create new event pulling thread
			new Thread(new Runnable() {
				@Override
				public void run() {
					while (true) {
						try {
							Thread.sleep(50);

						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						try {
							channel.pullEvent();
						} catch (IOException e) {
							e.printStackTrace();
							error(e.getMessage(), mRestart);
							break;
						}
					}
				}
			}).start();
			mPrinter = new Printer(channel.getInputStream(),
					channel.getOutputStream());
		} else {
			mPrinter = new Printer(mProtocolAdapter.getRawInputStream(),
					mProtocolAdapter.getRawOutputStream());
		}

		mPrinterInfo = mPrinter.getInformation();

		cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				 //printe receipt
				if(sales_type.equals("1")){
				printReceipt();
				}else if (sales_type.equals("2")){
					printCreditInvoice();
				}

				// toast("Setting up printer name and icon");
				// ((ImageView)findViewById(R.id.icon)).setImageResource(R.drawable.icon);
				// ((TextView)findViewById(R.id.name)).setText(mPrinterInfo.getName());
			}
		});
	}// end of init Printer
private void printCreditInvoice(){
	doJob(new Runnable() {
		@Override
		public void run() { 

			try {
				if (DEBUG)
					Log.d(LOG_TAG, "Print Page");
				mPrinter.reset();
				mPrinter.selectPageMode();
				
				
				int x = 0, y = 0;
				mPrinter.setPageRegion(0, y, 380, 105, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}{center}"+company_name+" {br}{center} "+address+"{br}{center}email:"+email+"{br}");
				y+=105;
				mPrinter.setPageRegion(0, y, 190, 35, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}{right}PIN:"+pin+"{/right}{br}");

				mPrinter.setPageRegion(190, y, 190, 70, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}VAT NO:"+vat_no+"{br}");
				y+=35;
				mPrinter.setPageRegion(0, y, 380, 35, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}{center}Tel :"+telephone+"{br}");
				y+=35;
				mPrinter.setPageRegion(0, y, 380, 35, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}{right}DATE:"+date+"{/right}{br}");

			 
				
				
				if (cashier_name.length() > 15) {
					y += 70;
				} else {
					y += 35;
				}
				mPrinter.setPageRegion(0, y, 250, 35, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}{right}Cashier:"+cashier_name+"{/right}{br}");

				mPrinter.setPageRegion(250, y, 130, 35, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}Register:"+register+"{br}");
				
				y+=35;
				mPrinter.setPageRegion(0, y, 380, 35, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}Invoice #:"+transaction+"{br}");
				 
			 
				if (customer_names.length() > 15) {
					y += 70;
				} else {
					y += 35;
				}
				mPrinter.setPageRegion(0, y, 250, 35, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}{right}Customer :"+customer_names+"{/right}{br}");
				
				y+=35;
				 
				mPrinter.setPageRegion(0, y, 230, 70, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}Item Name  {br}");

				mPrinter.setPageRegion(230, y, 50, 70,
						Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset} QTY {br}");

				mPrinter.setPageRegion(280, y, 100, 70,
						Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset} Total {br}");
				y+=20;
				mPrinter.setPageRegion(0, y, 380, 5, Printer.PAGE_LEFT);
				mPrinter.drawPageFrame(0, 0, 380, 5, Printer.FILL_BLACK, 3);
				
				
				y+=20;
				
				for (int i = 0; i < print_items.length(); i++) {
					bill_item = new JSONObject();
					try {
						bill_item = print_items.getJSONObject(i);						 
						
						mPrinter.setPageRegion(0, y, 230, 70, Printer.PAGE_LEFT);
						mPrinter.printTaggedText("{reset}" + bill_item.getString("product_name") + "{br}");

						mPrinter.setPageRegion(230, y, 50, 70,
								Printer.PAGE_LEFT);
						mPrinter.printTaggedText("{reset}"+bill_item.getString("quantity")+"{br}");

						mPrinter.setPageRegion(280, y, 100, 70,
								Printer.PAGE_LEFT);
						mPrinter.printTaggedText("{reset}"+bill_item.getDouble("total")+"{br}");

						if (bill_item.getString("product_name").length() > 15) {
							y += 70;
						} else {
							y += 35;
						}
						
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			 
				//draw line to end itmes 
				mPrinter.setPageRegion(0, y, 380, 5, Printer.PAGE_LEFT);
				mPrinter.drawPageFrame(0, 0, 380, 5, Printer.FILL_BLACK, 3);
				y += 10;
				mPrinter.setPageRegion(0, y, 280, 70, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}{right}Tax(" + tax_percentage + "%):{/right}{br}");

				mPrinter.setPageRegion(280, y, 100, 70, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}" + tax_amount + "{br}");
			 
				y += 35;
				mPrinter.setPageRegion(0, y, 380, 5, Printer.PAGE_LEFT);
				mPrinter.drawPageFrame(0, 0, 380, 5, Printer.FILL_BLACK,3 );
				y += 10;
				//total
				mPrinter.setPageRegion(0, y, 280, 70, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}{right}Total(Kshs):{/right}{br}");

				mPrinter.setPageRegion(280, y, 100, 70, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}"+total+"{br}");
				y += 50;
				 
				mPrinter.setPageRegion(0, y, 380, 70, Printer.PAGE_LEFT);
				mPrinter.printTaggedText("{reset}{center}Thanks for shopping {br}{center} @2014  www.intelligent.co.ke{br}");
				 
				mPrinter.printPage();
				mPrinter.selectStandardMode();
				mPrinter.feedPaper(110);
				mPrinter.flush();
			} catch (IOException e) {
				e.printStackTrace();
				error("Error Printing Receipt " + e.getMessage() + ". "
						+ e.getMessage(), mRestart);
			}
		}
	},"Print New Receipt");
}
	
	//print cash sales receipt
	private void printReceipt() {
		doJob(new Runnable() {
			@Override
			public void run() { 

				try {
					if (DEBUG)
						Log.d(LOG_TAG, "Print Page");
					mPrinter.reset();
					mPrinter.selectPageMode();
					
					
					int x = 0, y = 0;
					mPrinter.setPageRegion(0, y, 380, 105, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{center}"+company_name+" {br}{center} "+address+"{br}{center}email:"+email+"{br}");
					y+=105;
					mPrinter.setPageRegion(0, y, 190, 35, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{right}PIN:"+pin+"{/right}{br}");

					mPrinter.setPageRegion(190, y, 190, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}VAT NO:"+vat_no+"{br}");
					y+=35;
					mPrinter.setPageRegion(0, y, 380, 35, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{center}Tel :"+telephone+"{br}");
					y+=35;
					mPrinter.setPageRegion(0, y, 380, 35, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{right}DATE:"+date+"{/right}{br}");

				 
					
					y+=35;
					mPrinter.setPageRegion(0, y, 380, 35, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}Transaction #:"+transaction+"{br}");
					 
					if (cashier_name.length() > 15) {
						y += 70;
					} else {
						y += 35;
					}
					mPrinter.setPageRegion(0, y, 250, 35, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{right}Cashier:"+cashier_name+"{/right}{br}");

					mPrinter.setPageRegion(250, y, 130, 35, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}Register:"+register+"{br}");
					y+=35;
					 
					mPrinter.setPageRegion(0, y, 230, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}Item Name  {br}");

					mPrinter.setPageRegion(230, y, 50, 70,
							Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset} QTY {br}");

					mPrinter.setPageRegion(280, y, 100, 70,
							Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset} Total {br}");
					y+=20;
					mPrinter.setPageRegion(0, y, 380, 5, Printer.PAGE_LEFT);
					mPrinter.drawPageFrame(0, 0, 380, 5, Printer.FILL_BLACK, 3);
					
					
					y+=20;
					
					for (int i = 0; i < print_items.length(); i++) {
						bill_item = new JSONObject();
						try {
							bill_item = print_items.getJSONObject(i);						 
							
							mPrinter.setPageRegion(0, y, 230, 70, Printer.PAGE_LEFT);
							mPrinter.printTaggedText("{reset}" + bill_item.getString("product_name") + "{br}");

							mPrinter.setPageRegion(230, y, 50, 70,
									Printer.PAGE_LEFT);
							mPrinter.printTaggedText("{reset}"+bill_item.getString("quantity")+"{br}");

							mPrinter.setPageRegion(280, y, 100, 70,
									Printer.PAGE_LEFT);
							mPrinter.printTaggedText("{reset}"+bill_item.getDouble("total")+"{br}");

							if (bill_item.getString("product_name").length() > 15) {
								y += 70;
							} else {
								y += 35;
							}
							
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				 
					//draw line to end itmes 
					mPrinter.setPageRegion(0, y, 380, 5, Printer.PAGE_LEFT);
					mPrinter.drawPageFrame(0, 0, 380, 5, Printer.FILL_BLACK, 3);
					y += 10;
					mPrinter.setPageRegion(0, y, 280, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{right}Tax(" + tax_percentage + "%):{/right}{br}");

					mPrinter.setPageRegion(280, y, 100, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}" + tax_amount + "{br}");
				 
					y += 35;
					mPrinter.setPageRegion(0, y, 380, 5, Printer.PAGE_LEFT);
					mPrinter.drawPageFrame(0, 0, 380, 5, Printer.FILL_BLACK,3 );
					y += 10;
					//total
					mPrinter.setPageRegion(0, y, 280, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{right}Total(Kshs):{/right}{br}");

					mPrinter.setPageRegion(280, y, 100, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}"+total+"{br}");
					y += 50;
					//Cash rendered
					mPrinter.setPageRegion(0, y, 280, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{right}Cash:{/right}{br}");

					mPrinter.setPageRegion(280, y, 100, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}"+cash_rendered+"{br}");
					y += 50;
					
					//Change 
					mPrinter.setPageRegion(0, y, 280, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{right}Change Due:{/right}{br}");

					mPrinter.setPageRegion(280, y, 100, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}"+change+"{br}");
					y+=50;
					mPrinter.setPageRegion(0, y, 380, 70, Printer.PAGE_LEFT);
					mPrinter.printTaggedText("{reset}{center}Thanks for shopping {br}{center} @2014  www.intelligent.co.ke{br}");
					 
					mPrinter.printPage();
					mPrinter.selectStandardMode();
					mPrinter.feedPaper(110);
					mPrinter.flush();
				} catch (IOException e) {
					e.printStackTrace();
					error("Error Printing Receipt " + e.getMessage() + ". "
							+ e.getMessage(), mRestart);
				}
			}
		},"Print New Receipt");
	}

	private void printText() {
		doJob(new Runnable() {
			@Override
			public void run() {
				StringBuffer sb = new StringBuffer();
				sb.append("{reset}{center}{w}mPOs Receipt");
				sb.append("{br}");
				sb.append("{br}");
				sb.append("{reset}{u}No. Product Name    Tax   Total {br}");

				double total = 0;
				for (int i = 0; i < print_items.length(); i++) {
					bill_item = new JSONObject();
					try {
						bill_item = print_items.getJSONObject(i);
						sb.append("{reset}" + bill_item.getInt("id") + "  "
								+ bill_item.getString("product_name") + " "
								+ bill_item.getDouble("tax") + "  "
								+ bill_item.getDouble("total") + " {br}");
						total += bill_item.getInt("total");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// sb.append("{reset}1. Sandwich Bread{br}");
				// sb.append("{reset}{right}{h}Ksh 20 A{br}");
				// sb.append("{reset}2. {u}Sandwich Bread 2{br}");
				// sb.append("{reset}{right}{h}Ksh 30 B{br}");
				// sb.append("{reset}3. {i}Sandwich Bread 3{br}");
				// sb.append("{reset}{right}{h}Ksh 40{br}");
				sb.append("{br}");
				sb.append("{reset}{right}{w}{h}TOTAL: {/w}Ksh " + total
						+ " {br}");
				sb.append("{br}");
				sb.append("{reset}{center}{s}Thank You! {br}");
				sb.append("{br}");
				sb.append("{reset}{center}{s}@intelligent 2014 {br}");

				try {

					mPrinter.reset();
					mPrinter.printTaggedText(sb.toString());
					mPrinter.feedPaper(110);
					mPrinter.flush();
					closeActiveConnection();
				} catch (IOException e) {
					e.printStackTrace();
					error("Error Printing Receipt " + e.getMessage(), mRestart);
				}
			}
		}, "Print New Receipt");

	}
	 
	private void error(final String text, boolean resetConnection) {
		if (resetConnection) {
			cordova.getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(cordova.getActivity(), text,
							Toast.LENGTH_SHORT).show();
				}
			});

		}
	}// end error

	private void doJob(final Runnable job, final String message) {
		// Start the job from main thread
		cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// Progress dialog available due job execution
				final ProgressDialog dialog = new ProgressDialog(cordova
						.getActivity());
				dialog.setTitle("Please Wait");
				dialog.setMessage(message);
				dialog.setCancelable(false);
				dialog.setCanceledOnTouchOutside(false);
				dialog.show();

				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							job.run();
						} finally {
							dialog.dismiss();
						}
					}
				});
				t.start();
			}
		});
	}// end of do job

	private void printSelfTest() {
		doJob(new Runnable() {
			@Override
			public void run() {
				try {
					Log.d("Self test", "Print Self Test");
					mPrinter.printSelfTest();
					mPrinter.flush();
				} catch (IOException e) {
					e.printStackTrace();
					error("Failed to print test. " + e.getMessage(), true);
				}
			}
		}, "Printing Self Test");
	}

	private synchronized void closeActiveConnection() {
		closePrinterConnection();
		closeBlutoothConnection();
		closeNetworkConnection();
		closePrinterServer();
	}

	private synchronized void closePrinterConnection() {
		if (mPrinter != null) {
			mPrinter.release();
		}

		if (mProtocolAdapter != null) {
			mProtocolAdapter.release();
		}
	}

	private synchronized void closePrinterServer() {
		closeNetworkConnection();

		// Close network server
		PrinterServer ps = mPrinterServer;
		mPrinterServer = null;
		if (ps != null) {
			// if (DEBUG) Log.d(LOG_TAG, "Close Network server");
			try {
				ps.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private synchronized void closeNetworkConnection() {
		// Close network connection
		Socket s = mPrinterSocket;
		mPrinterSocket = null;
		if (s != null) {
			if (DEBUG)
				Log.d(LOG_TAG, "Close Network socket");
			try {
				s.shutdownInput();
				s.shutdownOutput();
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private synchronized void closeBlutoothConnection() {
		// Close Bluetooth connection
		BluetoothSocket s = mBluetoothSocket;
		mBluetoothSocket = null;
		if (s != null) {
			if (DEBUG)
				Log.d(LOG_TAG, "Close Blutooth socket");
			try {
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
